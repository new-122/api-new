FROM python:3.6.10-buster
ADD application.py /
ADD requirements.txt /
RUN pip install -r ./requirements.txt
EXPOSE 5000
CMD [ "python", "./application.py" ]